a = ['b','d']
b = ['a','b', 'c', 'd']

# using recursion
def lcs(i, j):
    if(i < 2 and j < 4):
        
        if a[i] == None  or b[j] == None:
            return 0
        elif(a[i] == b[j]):
            return 1 + lcs(i+1, j+1)
        else:
            return max(lcs(i+1, j), lcs(i, j+1))

# using dynamic programming
def lcsDynamic(i, j):
    
    for i in range(alen+1): 
        for j in range(blen+1): 
            if matrix[i][j] == -1:
                if i == 0 or j == 0 : 
                    matrix[i][j] = 0
                elif a[i-1] == b[j-1]: 
                    matrix[i][j] = matrix[i-1][j-1]+1
                else: 
                    matrix[i][j] = max(matrix[i-1][j] , matrix[i][j-1]) 
    print(alen)
    print(blen)
    return matrix[alen][blen]


a = ['b','d']
b = ['a','b', 'c', 'd']
alen = len(a)
blen = len(b)     
matrix = [[-1]*(blen+1) for i in range(alen+1)] 
print(matrix)
print(alen)
print(lcsDynamic(blen, alen))
print(matrix)