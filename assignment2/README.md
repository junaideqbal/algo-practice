## Assignment 2
--------
### Implement Bubble Sort, Insertion Sort and Selection Sort in Python. Calculate the frequency of the innermost loop with different sizes of input n. Plot these graphs with the graphs of assignment-1.


### Algorithms

- [x] insertion sort
- [x] selection sort
- [x] bubble sort

![graph Image](graph.png)