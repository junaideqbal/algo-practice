# Selection Sort algorithm

def findSmallest(j, arr):
    arr = arr
    min = arr[j]
    index = -1
    for k in range(j, len(arr)):
        print("j iteration:==============",j)
        if arr[k] <= min:
            min = arr[k]
            index = k
        
    print("this is the minimum number index is: " ,index , "value is: ",min)
    return index

# @firstIndex this is the index of 1st element of unsorted aray
# @min this is the index of min element in unsorted array
def swapping(min, firstIndex, arr):
    value = arr[min]
    arr[min] = arr[firstIndex]
    arr[firstIndex] = value
    return arr


def selectionSort(arr):
    for i1st in range(len(arr)-1):
        print("point is at :",i1st)
        minIndex = findSmallest(i1st, arr)
        print("after find smallest:",arr)
        arr = swapping(minIndex, i1st, arr)
        print("after swap:",arr)
    return arr


# Main Body

x = [7, 3, 2, 8, 1]

y = selectionSort(x)

print("sorted:",y)


