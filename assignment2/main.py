# main just for practice

import math
from bubblesort import bubblesort as bSort
from insertionsort import insertionsort as iSort
from matplotlib import pyplot as plt
from matplotlib import style

style.use('ggplot')
# stores the values of n
n1 = []
# stores the values of 	logn
n2 = []
# stores the values of 	nlogn
n3 = []
# stores the values of 	N^2
n4 = []
# stores the values of E^2
n5 = []

# Initializing values after calculation
for i in range(1,17):
    n1.append(i)
    n2.append(math.log2(i))
    n3.append(i * math.log2(i))
    n4.append(i ** 2)
    n5.append(math.exp(i))

# calling sorting algorithms
toSort = [3,1,5,6,0,2,7,23, 78, 45, 8, 32, 56, 9,3,1]
# calling bubblesort algo
b = bSort()
barr = b.bubbleSort()
print("value from bubble sort", barr)

# calling insertionsort algo
iSorting = iSort()
iarr = iSorting.insertionSort()
print("value from insertion sort", iarr)
#calling mergesort algo

# graph ploting

plt.plot(n1,n2,linewidth = 3, label="logn")
plt.plot(n1,n1,linewidth = 3, label="n")
plt.plot(n1,n3,linewidth = 3, label="nlogn")
plt.plot(n1,n4,linewidth = 3, label="n^2")
plt.plot(n1,n5,linewidth = 3, label="E^n")
plt.plot(n1,iarr,linewidth = 3, label="Insertion sort")
plt.plot(n1,barr,linewidth = 3, label="bubble sort")
#plt.plot(loopArr2,loopArr2,linewidth = 3, label="Bubble sort")


# setting legends position on graph
plt.legend(loc='upper right')

# setting labels
plt.title('Comparison chart')
plt.ylabel('Growth')
plt.xlabel('Operations')

# setting max and min x-axis and y-axis values
plt.axis([0, 20, 0, 100])

# plot
plt.show()