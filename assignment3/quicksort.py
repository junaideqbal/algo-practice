# select lest most as pivot
# less then pivot is on left
# greater then pivot is on right

# left pivot
# 
def partition(arr, sPoint, ePoint):
    ePoint -= 1
    pivot = arr[ePoint]
    pindex = 0

    for i in range(sPoint, ePoint):
        
        if(arr[i] < pivot):
            x = arr[i]
            arr[i] = arr[pindex]
            arr[pindex] = x
            pindex += 1
    
    swap = arr[pindex]
    arr[pindex] = pivot
    arr[ePoint] = swap

    print("partition is: ", arr)

ar = [7, 2, 1, 6, 8, 5, 3, 4]
partition(ar, 0, len(ar))