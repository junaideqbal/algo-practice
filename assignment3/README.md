## Assignment 3
--------
### Implement merge Sort and quick Sort in Python. Calculate the frequency of the innermost loop with different sizes of input n. Plot these graphs with the graphs of assignment-1.


### Algorithms

- [x] Merge sort
- [ ] Quick sort

![graph Image](graph.png)