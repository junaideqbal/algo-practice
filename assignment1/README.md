## Assignment 1 (Time Complexity)
--------
### Plot graphs of O(lgn), O(n), O(nlgn), O(n^2), O(2^n) in both Excel and Python.

![graph Image](graph.png)